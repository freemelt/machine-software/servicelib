<!--
SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>

SPDX-License-Identifier: LGPL-3.0-only
-->

# Servicelib

Python package containing common helpers used by the service-layer of the freemelt machine software. 

# License
Copyright © 2019-2021 Freemelt AB <opensource@freemelt.com>
The Servicelib is under [GNU Lesser General Public License, Version 3](https://www.gnu.org/licenses/lgpl-3.0.en.html)