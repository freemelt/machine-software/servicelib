# SPDX-FileCopyrightText: 2024 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only

# Built-in
import doctest

# Package
import servicelib.config


def load_tests(loader, tests, ignore):
    tests.addTests(
        doctest.DocTestSuite(servicelib.config, optionflags=doctest.ELLIPSIS)
    )
    return tests
