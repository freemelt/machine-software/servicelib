# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only

# Built-in
import unittest
import json

# Project
from servicelib import mqtt


class TestMQTTMessage(unittest.TestCase):
    def test_tags(self):
        m = mqtt.MQTTMessage(
            topic="freemelt/0/Test",
            fields=dict(temperature=37.0),
            tags=dict(sensor="eGun"),
        )
        self.assertEqual(json.loads(m.payload)["_t"]["sensor"], "eGun")


class TestMQTTAttribute(unittest.TestCase):
    def test(self):
        pass  # TODO


if __name__ == "__main__":
    unittest.main()
