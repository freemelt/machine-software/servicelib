# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only

# Built-in
import unittest
import pkgutil
import importlib

# Package
import servicelib as pkg


class TestImports(unittest.TestCase):
    """Test if all subpackages and modules can be imported"""

    def test_imports(self):
        def onerror(m):
            raise ImportError(m)

        for m in pkgutil.walk_packages(
            pkg.__path__, prefix=pkg.__name__ + ".", onerror=onerror
        ):
            print("import", m.name)
            importlib.import_module(m.name)
