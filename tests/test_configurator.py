# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only

# Built-in
import unittest

# Project
from servicelib import Configurator


class TestConfigurator(unittest.TestCase):
    def setUp(self):
        st1 = "./configurator"
        st2 = "./tests/testdata"
        search_paths = [st1, st2]
        self.Config1 = Configurator.load_config("ESconfig.json", search_paths)
        self.Config2 = Configurator.load_config("ESconfig1.yml", search_paths)

    def test_getitem(self):
        self.assertEqual(self.Config1["Config:ElectronService:current"], 1)
        self.assertEqual(self.Config1["Config:ElectronService:currentDemand"], 1.4)
        self.assertEqual(self.Config1["Config:ElectronService:voltage"], 60)
        self.assertEqual(self.Config1["Config:ElectronService:test"], True)
        self.assertEqual(self.Config1["Config:ElectronService:name"], "my_string")

        self.assertEqual(self.Config2["Config:ElectronService:current"], 1)
        self.assertEqual(self.Config2["Config:ElectronService:currentDemand"], 1.4)
        self.assertEqual(self.Config2["Config:ElectronService:voltage"], 60)
        self.assertEqual(self.Config2["Config:ElectronService:test"], True)
        self.assertEqual(self.Config2["Config:ElectronService:name"], "my_string")

    def test_key_does_not_exist(self):
        with self.assertRaises(KeyError):
            self.Config1["Config:KeyThatDoesNotExist"]

    def test_default(self):
        # Test that the default value is taken
        self.assertEqual(self.Config1.get("Config:KeyThatDoesNotExist", 423), 423)

        # Make sure absence of default value behaves the same as "[key]"
        self.assertEqual(
            self.Config1.get("Config:ElectronService:voltage"),
            self.Config1["Config:ElectronService:voltage"],
        )

        # Verify wrong type of default value
        with self.assertRaises(TypeError) as context:
            self.Config1.get("Config:ElectronService:voltage", default="a string")
        self.assertTrue("does not agree" in str(context.exception))

    def test_get_str(self):
        self.assertIsInstance(
            self.Config2.get_str("Config:ElectronService:current"), str
        )
        self.assertEqual(self.Config2.get_str("Config:ElectronService:test"), "True")

    def test_get_bool(self):
        self.assertIsInstance(
            self.Config2.get_bool("Config:ElectronService:test"), bool
        )
        self.assertEqual(self.Config2.get_bool("Config:ElectronService:enabled"), True)
        self.assertEqual(
            self.Config2.get_bool("Config:ElectronService:disabled"), False
        )
        with self.assertRaises(TypeError):
            print(self.Config2.get_bool("Config:ElectronService:current"))

    def test_get_float(self):
        self.assertIsInstance(
            self.Config2.get_float("Config:ElectronService:current"), float
        )
        self.assertEqual(
            self.Config2.get_float("Config:ElectronService:duration"), 3.14
        )
        with self.assertRaises(TypeError):
            print(self.Config2.get_float("Config:ElectronService:enabled"))
        with self.assertRaises(TypeError):
            print(self.Config2.get_float("Config:ElectronService:name"))

    def test_get_int(self):
        self.assertIsInstance(
            self.Config2.get_int("Config:ElectronService:current"), int
        )
        self.assertEqual(self.Config2.get_int("Config:ElectronService:count"), 123)
        with self.assertRaises(TypeError):
            self.Config2.get_int("Config:ElectronService:currentDemand")
        with self.assertRaises(TypeError):
            self.Config2.get_int("Config:ElectronService:name")


if __name__ == "__main__":
    unittest.main()
