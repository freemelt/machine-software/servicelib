# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only

PKGNAME = $(shell dpkg-parsechangelog -S source)
VERSION = $(shell dpkg-parsechangelog -S version | sed 's/-.*//')
DIST = $(shell dpkg-parsechangelog -S distribution)
DEBRELEASE = $(shell awk -F. '{print "deb"$$1}' /etc/debian_version)

all:

servicelib/_version.py: servicelib/_version.py.in
	sed -e "s,[@]VERSION[@],$(VERSION),g" < servicelib/_version.py.in > servicelib/_version.py

release: servicelib/_version.py
	tar --numeric-owner --group 0 --owner 0 -cJh \
	  --xform "s,^,$(PKGNAME)-$(VERSION)/," \
	  -f $(PKGNAME)-$(VERSION).tar.xz \
	  servicelib/*.py tests/*.py tests/testdata/* setup.py README.md

deb-ci:
	if [ -z "$$CI_COMMIT_TAG" ]; then \
	  DEBFULLNAME="$$GITLAB_USER_NAME" DEBEMAIL="$$GITLAB_USER_EMAIL" \
	   dch -l+$(DEBRELEASE)~git.$$CI_PIPELINE_ID.$$CI_COMMIT_SHORT_SHA "Untagged build" -D unstable; \
	else \
	  if [ "$$CI_COMMIT_TAG" != "$(VERSION)" ] && [ "$$CI_COMMIT_TAG" != "v$(VERSION)" ]; then \
	     echo "debian/changelog has not been updated to the new version!"; \
	     exit 1; \
	  fi; \
	  if [ "$(DIST)" != "stable" ]; then \
	     echo "debian/changelog must use stable for tagged releases (DIST=$$DIST)!"; \
	     exit 1; \
	  fi; \
	  DEBFULLNAME="$$GITLAB_USER_NAME" DEBEMAIL="$$GITLAB_USER_EMAIL" \
	   dch -l+$(DEBRELEASE). --no-auto-nmu -D $(DIST) 'Automatic CI build'; \
	fi

deb: release
	rm -rf ./$(PKGNAME)-$(VERSION)
	tar -xJf $(PKGNAME)-$(VERSION).tar.xz
	ln -sf $(PKGNAME)-$(VERSION).tar.xz \
	  $(PKGNAME)_$(VERSION).orig.tar.xz
	cp -a debian/ $(PKGNAME)-$(VERSION)/
	(cd $(PKGNAME)-$(VERSION) && DEB_BUILD_OPTIONS=noddebs dpkg-buildpackage -us -uc -b)

docs: release
	(cd docs && make html)

clean:
	-rm -rf freemelt-servicelib[-_]*
