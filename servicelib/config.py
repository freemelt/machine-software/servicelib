# SPDX-FileCopyrightText: 2024 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only
"""Support for the new TOML-based configuration scheme

The old .yaml file is now a legacy configuration file.  We first load
defaults from /usr/share/freemelt/conf.d/*.toml, followed by
/etc/freemelt/conf.d/*.toml and finally /etc/freemelt/<legacy>.yaml.

Applications should add command line arguments to print the config
with --show-config or --full-config (which also include all defaults).

If CONFIGURATION_DIRECTORY is set, then it is used instead of
/etc/freemelt/conf.d.

The old .yaml file is now a legacy configuration file and the primary
way to configure the Freemelt software is now to use toml files in
/etc/.

Functions:

    - read_legacy: Reads and returns legacy YAML config.
    - read_toml: Reads and returns TOML config.
    - update: Merges config updates into a base config.
    - read_dir: Reads and merges TOML configs from a directory.
    - read_config: Reads and returns the full application config.
    - read_config_defaults: Reads and returns the default config.
    - diff_configs: Computes and returns the difference between two
      configs.
    - show_toml: Generates and returns a TOML string representing the
      differences between a config and its defaults.
    - add_cli_arguments: Adds config-related arguments to an argparse
      CLI parser.
    - add_cli_arguments_legacy: Adds legacy config-related arguments
      to an argparse CLI parser.
    - handle_config: Handles the application configuration based on
      CLI arguments and legacy configurations.

Classes:

    - Config: A simple config wrapper allowing hierarchical access to
      config data: `Config["key1:key2:key3"]`.
"""

# Built-in
import argparse
import collections
import json
import logging
import os
import pathlib
from collections.abc import MutableMapping
from typing import Optional, Callable, Any

# Third-party
import yaml
import tomlkit
import tomlkit.exceptions

USR_CONFIG_DIR = "/usr/share/freemelt/conf.d"
USR_CONFIG_DIR_ENV = "DEFAULT_CONFIGURATION_DIRECTORY"
ETC_CONFIG_DIR = "/etc/freemelt/conf.d"
ETC_CONFIG_DIR_ENV = "CONFIGURATION_DIRECTORY"
# For reference:
# https://www.freedesktop.org/software/systemd/man/latest/systemd.exec.html#RuntimeDirectory=
LOG = logging.getLogger("config")


def read_legacy(filename: pathlib.Path, quiet: bool) -> dict:
    """Read legacy yaml file (normally in /etc/freemelt/)"""
    if not quiet:
        LOG.warning(
            "Loading legacy file %s. Please migrate to TOML configuration scheme.",
            filename,
        )
    with open(filename, "r") as fh:
        return yaml.safe_load(fh)


def read_toml(filename: pathlib.Path, quiet: bool) -> dict:
    f"""Read toml file (normally in {ETC_CONFIG_DIR})"""
    if not quiet:
        LOG.debug("Loading %s", filename)
    filename = pathlib.Path(filename)
    try:
        return tomlkit.parse(filename.read_text())
    except tomlkit.exceptions.TOMLKitError as err:
        raise tomlkit.exceptions.TOMLKitError(f"{filename}: {err}") from None


def filename_sort(filepath: pathlib.Path) -> tuple[float, str]:
    """Custom sort function for toml filenames

    This decides the order in which config files are merged.
    Sorted by numerical prefix first, then alphabetically.

    >>> files = [
    ...    "20-bbbb.toml",
    ...    "20-aaaa.toml",
    ...    "cccc.toml",
    ...    "10-bbbb.toml",
    ... ]
    >>> files.sort(key=filename_sort)
    >>> files
    ['10-bbbb.toml', '20-aaaa.toml', '20-bbbb.toml', 'cccc.toml']
    """
    filepath = pathlib.Path(filepath)
    base_name = filepath.name
    prefix, _, _ = base_name.partition("-")

    # Attempt to convert prefix to integer for numerical comparison
    try:
        return float(int(prefix)), base_name
    except ValueError:
        return float("inf"), base_name


def update(
    base: MutableMapping, updates: MutableMapping, extend_ok: bool = False
) -> MutableMapping:
    """Recursively merge `updates` into `base`.

    This function recursively updates the `base` mapping with
    key-value pairs from `updates`.  If `extend_ok` is False
    (default), keys in `updates` that do not exist in `base` are
    ignored.  If `extend_ok` is True, new keys from `updates` are
    added to `base`.

    Parameters:
        - base (MutableMapping): The dictionary to be updated, acting
          as the base.
        - updates (MutableMapping): The dictionary containing updates
          to be merged into `base`.
        - extend_ok (bool): Flag to allow (`True`) or disallow
          (`False`) adding new keys to `base`.  Defaults to False.

    Raises:
        - ValueError: When there's a type conflict for a key (i.e.,
          attempting to merge a mapping with a non-mapping value or
          vice versa).

    Returns: MutableMapping: The updated mapping after merging
    `updates` into `base`.

    Examples:

    >>> base = {
    ...    "a": {
    ...        "f1": 3.14,
    ...        "b": {
    ...            "f2": [1, 2, {"d": 3}],
    ...            "c": {
    ...                "f3": "hello"
    ...            }
    ...        }
    ...    }
    ... }
    >>> update(base, {"a": {"f1": 6.18}})
    {'a': {'f1': 6.18, 'b': {'f2': [1, 2, {'d': 3}], 'c': {'f3': 'hello'}}}}

    >>> update(base, {"a": {"b": {"f2": [4]}}})
    {'a': {'f1': 6.18, 'b': {'f2': [4], 'c': {'f3': 'hello'}}}}

    >>> update(base, {"e": "new fields are ignored"})
    {'a': {'f1': 6.18, 'b': {'f2': [4], 'c': {'f3': 'hello'}}}}

    >>> update(base, {"a": {"f1": 666, "e": "new fields are ignored"}})
    {'a': {'f1': 666, 'b': {'f2': [4], 'c': {'f3': 'hello'}}}}

    >>> update(base, {"e": "new field OK"}, extend_ok=True)  # "e" added
    {'a': {'f1': 666, 'b': {'f2': [4], 'c': {'f3': 'hello'}}}, 'e': 'new field OK'}

    >>> try:
    ...     update(base, {"a": 123})
    ... except ValueError as err:
    ...     err
    ValueError("Mapping and non-mapping value conflict on key 'a'")
    """
    for key, value in updates.items():
        # When extend_ok is False and key is not in base, ignore key.
        if not extend_ok and key not in base:
            continue
        # Check for type conflict when existing value is a mapping
        # but the new value is not, and vice versa.
        if key in base:
            a = isinstance(base[key], MutableMapping)
            b = isinstance(value, MutableMapping)
            if a ^ b:
                raise ValueError(
                    f"Mapping and non-mapping value conflict on key {key!r}"
                )
        if isinstance(value, MutableMapping):
            # If the value is a mapping, recursively update or add
            # the sub-mapping. Initiate the sub-mapping if key does not exist.
            base[key] = update(base.get(key, type(base)()), value, extend_ok)
        else:
            # For non-mapping types or when not extending, directly
            # set/update the key-value pair.
            base[key] = value
    return base


def read_dir(dirname: pathlib.Path, quiet: bool) -> dict:
    """Find and merge all toml files.

    Searches through a given directory and its subdirectories for all
    TOML files, ignoring hidden ones.  Merges the contents of these
    files into a single dictionary.

    Args:
        - dirname (pathlib.Path): The directory path to search within.
        - quiet (bool): If True, don't print config filepaths loaded.

    Returns:
        - dict: A dictionary containing the merged data from all found
          TOML files.
    """
    files = list()
    for path in dirname.rglob("*.toml"):
        if path.is_file() and not path.name.startswith("."):
            files.append(path)

    files.sort(key=filename_sort)

    cfg = dict()
    for file in files:
        data = read_toml(file, quiet)
        update(cfg, data, extend_ok=True)
    return cfg


def read_dirs(dirnames: str, quiet: bool) -> dict:
    """Repeated calls of read_dir() on :-separated dirnames"""
    cfg = dict()
    for dir in dirnames.strip().split(os.pathsep):
        data = read_dir(pathlib.Path(dir), quiet)
        update(cfg, data, extend_ok=True)
    return cfg


class Config(collections.UserDict):
    # A simpler version of `servicelib.Configurator`

    def __init__(self, dict=None, /, **kwargs):
        super().__init__(dict, **kwargs)
        # It is assumed to be very uncommon for TOML field names to
        # include the ":" character.
        self.delimiter = ":"

    @property
    def block(self) -> dict:  # For backwards compatibility
        return self.data

    def __getitem__(self, key: str):
        """Get config data by specifying a "key1:key2:key3:..:keyN"-key.

        >>> c = Config({"a":{"b":123}})
        >>> c["a:b"]
        123

        >>> try:
        ...     c["a:c"]
        ... except KeyError as err:
        ...     err
        KeyError('a:c')

        >>> try:
        ...     c["a:b:c"]
        ... except KeyError as err:
        ...     err
        KeyError('a:b:c')

        >>> type(c["a"]).__name__
        'Config'

        :param key: A key of the form "key1:key2:key3:..:keyN"
        :return: Config data value, corresponding to the N:th key of element.
        """
        parts = key.split(self.delimiter, maxsplit=1)
        try:
            data = self.data[parts[0]]
            if len(parts) == 1:
                if isinstance(data, MutableMapping):
                    return self.__class__(data)
                return data
            if isinstance(data, MutableMapping):
                return self.__class__(data)[parts[1]]
            if parts[1]:
                raise KeyError(key)
            return data
        except KeyError:
            raise KeyError(key) from None

    def get_str(self, key: str, default: Optional[str] = None) -> str:
        """Get string value

        >>> c = Config({"a": "string"})
        >>> c.get_str("a")
        'string'

        >>> c = Config({"a": "string"})
        >>> c.get_str("b", "default")
        'default'
        """
        try:
            return str(self[key])
        except KeyError as err:
            if default is not None:
                return str(default)
            raise err from None

    def get_bool(self, key: str, default: Optional[bool] = None) -> bool:
        """Get bool value

        >>> c = Config({"a": True})
        >>> c.get_bool("a")
        True

        >>> c = Config({"a": True})
        >>> c.get_bool("b", False)
        False
        """
        try:
            return bool(self[key])
        except KeyError as err:
            if default is not None:
                return bool(default)
            raise err from None

    def get_float(self, key: str, default: Optional[float] = None) -> float:
        """Get float value

        >>> c = Config({"a": 3.14})
        >>> c.get_float("a")
        3.14

        >>> c = Config({"a": 3.14})
        >>> c.get_float("b", 6.18)
        6.18
        """
        try:
            return float(self[key])
        except KeyError as err:
            if default is not None:
                return float(default)
            raise err from None

    def get_int(self, key: str, default: Optional[int] = None) -> int:
        """Get int value

        >>> c = Config({"a": 123})
        >>> c.get_int("a")
        123

        >>> c = Config({"a": 123})
        >>> c.get_int("b", 999)
        999
        """
        try:
            return int(self[key])
        except KeyError as err:
            if default is not None:
                return int(default)
            raise err from None


def read_config_defaults(
    dirnames: str = os.getenv(USR_CONFIG_DIR_ENV, USR_CONFIG_DIR),
    quiet: bool = True,
) -> Config:
    """Return default config"""
    return Config(read_dirs(dirnames, quiet))


def read_config_customizations(
    dirnames: str = os.getenv(ETC_CONFIG_DIR_ENV, ETC_CONFIG_DIR),
    quiet: bool = True,
) -> Config:
    """Return customized config"""
    return Config(read_dirs(dirnames, quiet))


def read_config(quiet: bool = True) -> Config:
    """Return the merged config of defaults and custimizations"""
    cfg_def = read_config_defaults(quiet=quiet)
    cfg_cus = read_config_customizations(quiet=quiet)
    cfg = update(cfg_def, cfg_cus, extend_ok=False)
    return Config(cfg)


def update_with_legacy_config(
    cfg: MutableMapping,
    legacy_cfg_file: pathlib.Path,
    legacy_cfg_postprocess: Callable[[dict], dict] = lambda d: d,
    quiet: bool = True,
) -> None:
    """Merge legacy config into an existing config"""
    data = read_legacy(legacy_cfg_file, quiet)
    data = legacy_cfg_postprocess(data)
    update(cfg, data, extend_ok=False)


def diff_configs(src: MutableMapping, dst: MutableMapping) -> MutableMapping:
    """Return the subset of `src` that differs from `dst`.

    Example:

    >>> src_dict = {"name": "John", "age": 30, "details": {"height": 175, "weight": 70}}
    >>> dst_dict = {"name": "John", "age": 32, "details": {"height": 175, "weight": 75}}
    >>> diff_configs(src_dict, dst_dict)
    {'age': 30, 'details': {'weight': 70}}
    """
    result = type(src)()
    for key, src_val in src.items():
        dst_val = dst.get(key)
        # If both are mappings, recurse.
        if isinstance(src_val, MutableMapping) and isinstance(dst_val, MutableMapping):
            nested_result = diff_configs(src_val, dst_val)
            if nested_result:
                result[key] = nested_result
        elif src_val != dst_val:
            result[key] = src_val
    return result


def show_toml(cfg: MutableMapping, defaults: MutableMapping) -> str:
    """Print toml of what differs in cfg compared to defaults

    Examples:

    >>> cfg = {
    ...     "database": {
    ...         "connection_max": 5000,
    ...         "enabled": False,
    ...         "ports": [8001],
    ...         "server": "192.168.1.1"
    ...     }
    ... }

    >>> defaults = {
    ...     "database": {
    ...         "connection_max": 5000,
    ...         "enabled": True,
    ...         "ports": [8001, 8002],
    ...         "server": "192.168.1.1"
    ...     },
    ...     "extra": 123
    ... }

    >>> print(show_toml(cfg, dict()).strip())
    [database]
    connection_max = 5000
    enabled = false
    ports = [8001]
    server = "192.168.1.1"

    >>> print(show_toml(cfg, defaults).strip())
    [database]
    enabled = false
    ports = [8001]
    """
    diff = diff_configs(cfg, defaults)

    # tomlkit only supports dict type for some reason.
    # Convert all Config back to dicts:
    def _custom_serializer(obj):
        if isinstance(obj, Config):
            return dict(obj)
        return obj

    diff = json.loads(json.dumps(diff, default=_custom_serializer))
    return tomlkit.dumps(diff)


def add_cli_arguments(parser: argparse.ArgumentParser) -> None:
    """Add config related arguments to parser"""
    parser.add_argument(
        "--show-config",
        action="store_true",
        help="Show the configuration and exit (diff from defaults).",
    )
    parser.add_argument(
        "--full-config",
        action="store_true",
        help="Show the full configuration and exit.",
    )
    # Todo: "--edit-config"


def add_cli_arguments_legacy(
    parser: argparse.ArgumentParser, default: pathlib.Path
) -> None:
    parser.add_argument(
        "--legacy-config",
        type=pathlib.Path,
        default=default,
        metavar="PATH",
        help="Path to legacy config file (default: %(default)s).",
    )


def _nested_dict(colonstring: str, value: Any) -> dict:
    """Turn colon-separated string into nested dict with final value

    >>> _nested_dict("a:b:c", 123)
    {'a': {'b': {'c': 123}}}

    >>> _nested_dict("a", 123)
    {'a': 123}
    """
    keys = colonstring.split(":")
    nested_dict = {}
    temp_dict = nested_dict
    for key in keys[:-1]:  # Iterate through all but the last key
        temp_dict[key] = {}  # Create a new dictionary for the current key
        temp_dict = temp_dict[key]  # Move to the newly created dictionary
    temp_dict[keys[-1]] = value
    return nested_dict


def handle_config(
    args: argparse.Namespace,
    primarykey: str = "",
    legacy_cfg_postprocess: Callable[[dict], dict] = lambda d: d,
) -> Optional[Config]:
    """Returns application config or prints it based on CLI arguments.

    Parses Command Line Interface (CLI) arguments to either return the
    application's config object or print the configuration to stdout
    and exit.

    Parameters:
        args: Parsed CLI arguments.
        primarykey: Optional. Specifies a key to select a subset of the
          config for processing. Defaults to an empty string, meaning
          the entire config is considered.
        legacy_cfg_postprocess: A function to post-process legacy
          configuration data after loading but before merging with
          the primary configuration. This allows for renaming fields
          or restructuring the data format to be compatible with the
          current configuration schema.

    Returns:
        Config: The full or filtered application configuration if the
        CLI arguments did not request displaying the configuration; otherwise,
        None is returned after printing the configuration to stdout.

    Behavior based on CLI arguments:
        - If `--show-config` is specified, prints the difference between the
          current configuration and default values.
        - If `--full-config` is specified, prints the complete current
          configuration.
        - Optionally, `--legacy-config` followed by a path allows merging a
          specified legacy YAML configuration file.
    """
    # Get full config and optionally merge the legacy config
    cfg = read_config(quiet=False)
    if legacy_config := getattr(args, "legacy_config", None):
        try:
            update_with_legacy_config(cfg, legacy_config, legacy_cfg_postprocess)
        except FileNotFoundError:
            LOG.debug("Legacy file %s does not exist. Ignored.", legacy_config)
    if not (args.show_config or args.full_config):
        return cfg

    # Handle "config printing" options "--show-config" and "--full-config"
    if args.show_config:
        def_cfg = read_config_defaults()
        if isinstance(args.show_config, bool) and primarykey:
            # Reduce output to selected key
            cfg = _nested_dict(primarykey, cfg[primarykey])
            def_cfg = _nested_dict(primarykey, def_cfg[primarykey])
        elif isinstance(args.show_config, str):
            cfg = _nested_dict(args.show_config, cfg[args.show_config])
            def_cfg = _nested_dict(args.show_config, def_cfg[args.show_config])
    if args.full_config:
        def_cfg = collections.defaultdict(object)  # Unequal to anything
        if isinstance(args.full_config, bool) and primarykey:
            cfg = _nested_dict(primarykey, cfg[primarykey])
        elif isinstance(args.full_config, str):
            cfg = _nested_dict(args.full_config, cfg[args.full_config])

    text = show_toml(cfg, def_cfg)
    try:
        # Print with colors
        import rich  # Third-party
        import rich.syntax

        syntax = rich.syntax.Syntax(text, "toml")
        rich.print(syntax)
    except ModuleNotFoundError:
        print(text)
    return None


def main() -> None:
    """CLI for debuging and testing the configuration scheme.

    Run `python3 -m servicelib.config --help` for usage.
    """
    import sys

    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--show-config",
        nargs="?",
        default=False,  # Used when --show-config is not specified at all
        const=True,  # Used when --show-config is specified without an argument
        metavar="KEY",  # E.g. --show-config ElectronService
        help="Show the configuration and exit (diff from defaults).",
    )
    parser.add_argument(
        "--full-config",
        nargs="?",
        default=False,  # Used when --full-config is not specified at all
        const=True,  # Used when --full-config is specified without an argument
        metavar="KEY",  # E.g. --show-config ElectronService
        help="Show the full configuration and exit.",
    )
    parser.add_argument(  # Remove in the future
        "--legacy-config",
        type=pathlib.Path,
        metavar="PATH",
        help="Path to legacy YAML config file.",
    )
    args = parser.parse_args()
    try:
        cfg = handle_config(args)
    except Exception as err:
        LOG.error("%s: %s", type(err).__name__, err)
        sys.exit(1)
    if cfg is None:
        sys.exit(0)


if __name__ == "__main__":
    main()
