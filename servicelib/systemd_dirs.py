# SPDX-FileCopyrightText: 2019-2024 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only
"""This module provides a dataclass for handling systemd directory paths

For more information, see:
https://www.freedesktop.org/software/systemd/man/latest/systemd.exec.html#RuntimeDirectory=
"""

# Built-in
import dataclasses
import pathlib
import os


@dataclasses.dataclass
class SystemdDirectories:
    state: pathlib.Path
    cache: pathlib.Path
    logs: pathlib.Path
    runtime: pathlib.Path
    configuration: pathlib.Path

    @staticmethod
    def from_env(default_subdir: str) -> "SystemdDirectories":
        def get_path(env_var: str, default_path: str) -> pathlib.Path:
            return pathlib.Path(os.getenv(env_var, default_path).split(":")[0])

        return SystemdDirectories(
            state=get_path("STATE_DIRECTORY", f"/var/lib/{default_subdir}"),
            cache=get_path("CACHE_DIRECTORY", f"/var/cache/{default_subdir}"),
            logs=get_path("LOGS_DIRECTORY", f"/var/log/{default_subdir}"),
            runtime=get_path("RUNTIME_DIRECTORY", f"/run/{default_subdir}"),
            configuration=get_path("CONFIGURATION_DIRECTORY", f"/etc/{default_subdir}"),
        )
