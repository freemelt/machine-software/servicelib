# SPDX-FileCopyrightText: 2019-2024 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: LGPL-3.0-only
"""This module provides a systemd watchdog kicker coroutine.

Useful for ensuring that the asyncio event loop is not blocked.

For more information, see:
https://www.freedesktop.org/software/systemd/man/255/sd_watchdog_enabled.html
"""

# Built-in
import asyncio
import logging
import os

# Third-party
from systemd.daemon import notify


async def systemd_watchdog(logger_name: str = "") -> None:
    """Periodically notify systemd's watchdog to prevent the service from being killed."""
    log = logging.getLogger(logger_name)
    watchdog_usec = os.getenv("WATCHDOG_USEC", 0)
    if not watchdog_usec:
        log.warning("systemd WatchdogSec= not configured")
        return
    try:
        us = int(watchdog_usec)
    except ValueError:
        log.error("Invalid value for systemd WatchdogSec= %r", watchdog_usec)
        return

    # Ensure interval is less than half to be safe and convert to sec
    interval = us / (2.1 * 1e6)
    log.debug("Kicking systemd watchdog every %.1f seconds", interval)

    while True:
        notify("WATCHDOG=1")
        await asyncio.sleep(interval)
