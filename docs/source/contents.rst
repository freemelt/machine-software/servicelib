.. configurator documentation master file, created by
   sphinx-quickstart on Wed Jun 12 11:13:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Module Reference
----------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: servicelib.configurator

.. class:: servicelib.configurator.Configurator

   .. automethod:: load_config
   .. automethod:: __getitem__
   .. automethod:: get
   .. automethod:: get_str
   .. automethod:: get_float
   .. automethod:: get_bool
   .. automethod:: get_int
   .. automethod:: __init__


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
