<!--
SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>

SPDX-License-Identifier: LGPL-3.0-only
-->

# Welcome to configurator's documentation

This project contains the `configurator` module which is used to find
and load service configuration files. **Example**:

```python
>>> # Create configurator
>>> c = Configurator.load_config('vtservice.yaml', search_paths=['/etc/freemelt', '.'])
>>> # Get value by ":"-separated key. (delimiter can be configured)
>>> c['Config:OPC:Pressure:eGunPressure_GET']
1.234
```

## Installation
Make sure you are standing in the project directory. Run this command to
install the module in your current Python interpreter.
```bash
$ python setup.py install
```
Alternatively you can create a source distribution (a .tar.gz file).
```bash
$ python setup.py sdist
```
The output file will be located in the created `dist/` directory.
Copy that file to the computer where you need to make the install and run
```bash
$ python -m pip install ./configurator-<insert version here>.tar.gz
```

## Test & Documentation

This is how you run the tests and build the documentation. First, install
the requirements:

```bash
$ python -m pip install -r requirements.txt
```

Now you can run the tests:

```bash
$ nose2 --verbose --with-coverage
```
And you will see an output (at the time of this writing) similar to this:
```
test_default (test_Configurator.TestConfigurator) ... ok
test_get_bool (test_Configurator.TestConfigurator) ... ok
test_get_float (test_Configurator.TestConfigurator) ... ok
test_get_int (test_Configurator.TestConfigurator) ... ok
test_get_str (test_Configurator.TestConfigurator) ... ok
test_getitem (test_Configurator.TestConfigurator) ... ok
test_key_does_not_exist (test_Configurator.TestConfigurator) ... ok

----------------------------------------------------------------------
Ran 7 tests in 0.012s

OK
Name                        Stmts   Miss  Cover
-----------------------------------------------
configurator.py                81      5    94%
setup.py                        2      2     0%
test/test_Configurator.py      54      1    98%
-----------------------------------------------
TOTAL                         137      8    94%
```

Build the documentation by running

```bash
$ cd docs
$ make html
```
and then open `docs/build/index.html` in your browser.
